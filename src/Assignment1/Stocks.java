package Assignment1;

import java.util.Random;

public class Stocks {

    private StocksPair[] stocks;

    public Stocks() {
        this.stocks = new StocksPair[]{ new StocksPair( Stock.Apple ),
                                        new StocksPair( Stock.BP ),
                                        new StocksPair( Stock.Cisco ),
                                        new StocksPair( Stock.Dell ),
                                        new StocksPair( Stock.Ericsson ) };
    }

    public StocksPair getPair( Stock stock ) {
        for( StocksPair stocksPair : stocks ) {
            if( stocksPair.getStock().equals( stock ) )
                return stocksPair;
        }
        return null;
    }

    public StocksPair[] getStocks() {
        return this.stocks.clone();
    }

    public PlayerShares[] setPlayerShares() {
        PlayerShares[] newStocks = new PlayerShares[ this.stocks.length ];
        int[] shares = new int[] { 0, 0, 0, 0, 0 };
        Random random = new Random();

        for( int i = 0; i < 10; i++ ) shares[ random.nextInt( 5 ) ]++;

        for( int i = 0; i < this.stocks.length; i++ ) {
            newStocks[i] = new PlayerShares( this.stocks[i], shares[ i ] );
        }
        return newStocks;
    }

    public PlayerShares[] setPlayerShares(int[] shares ) {
        PlayerShares[] newStocks = new PlayerShares[ this.stocks.length ];

        for( int i = 0; i < this.stocks.length; i ++ ) {
            newStocks[ i ] = new PlayerShares( this.stocks[i], shares[ i ] );
        }

        return newStocks;
    }
}
