package Assignment1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {

    public List< Card > cards;
    public Stock stock;

    public Deck( Stock stock ) {
        this.stock = stock;
        this.cards = new ArrayList<>();

        for( int effect : Card.EFFECTS ) this.cards.add( new Card( effect ) );
    }

    public Deck( Stock stock, int ... effects ) {

        this.stock = stock;
        this.cards = new ArrayList<>();

        for( int effect : effects ) this.cards.add( new Card( effect ) );
    }

    public Card drawCard() {
        Card card = cards.get( 0 );
        cards.remove( 0 );
        return card;
    }

    public void shuffle() {
        Collections.shuffle( this.cards );
    }

    public Stock getStock() {   return this.stock;    }

    @Override
    public String toString() {
        return stock + " " + cards;
    }
}

class Card implements Comparable< Card > {

    public static final int[] EFFECTS = new int[] { -20, -10, -5, 5, 10, 20 };

    private int effect;

    public Card( int effect ) {
        this.effect = effect;
    }

    public int getEffect() { return this.effect; }

    @Override
    public String toString() {
        return "" + effect;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return effect == ((Card) obj).effect;
    }

    @Override
    public int compareTo( Card other ) {
        return Integer.compare( effect, other.effect );
    }

    public static Card parse( String s ) {
        try {
            return new Card( Integer.parseInt( s.trim() ) );
        } catch( NumberFormatException e ) {
            return null;
        }
    }
}
