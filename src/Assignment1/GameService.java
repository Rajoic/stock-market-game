package Assignment1;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class GameService extends Thread {

    private static Game game;

    public static int maxPlayers = 1;

    private static Stocks stocks;
    private static int round;
    private static CyclicBarrier cb;
    private static int[] totalVotesFromLastRound = new int[] { 0, 0, 0, 0, 0 };

    private ServerSocket serverSocket;
    private final int SERVER_SOCKET_PORT = 8080;
    private ArrayList< GameClient > players;

    public GameService() {
        try {
            serverSocket = new ServerSocket( SERVER_SOCKET_PORT );
        } catch (IOException e) {
            e.printStackTrace();
            System.exit( 1 );
        }

        players = new ArrayList<>();
        stocks = new Stocks();
        GameClient.setStocks( stocks );
        Request.stocks = GameService.stocks;

    }

    private void connectPlayers( int numberOfPlayers ) {
        GameClient service;
        while( numberOfPlayers <= maxPlayers ) {
            try {
                service = new GameClient( serverSocket.accept(), numberOfPlayers, stocks.setPlayerShares(), game);
                service.start();
                System.out.println( numberOfPlayers );
                players.add( service );
                System.out.println( "Player " + ( numberOfPlayers ) + " connected!" );

                if( numberOfPlayers == 1 ) {
                    cb = new CyclicBarrier(2);
                    // GameClient.setBarrier( cb );
                    cb.await();
                }

            } catch ( Exception e ) { e.printStackTrace(); }
            numberOfPlayers++;
        }
    }

    public void endPhase() {
        round++;
        GameClient.incrementRound();
        GameService.totalVotesFromLastRound = game.getTotalVotes();
        try { cb.await(); } catch ( Exception e ) { e.printStackTrace(); }
    }

    public void preGameActions() {
        for (Deck deck : game.decks) deck.shuffle();
        game.drawCards();
        round = 1;
        GameClient.setRound( round );
        System.out.println(maxPlayers + " players");
        try {
            cb.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        cb = new CyclicBarrier( ( maxPlayers + 1 ) );
        while( round < 6 ) {
            try {
                System.out.println("HELLO");
                cb.await();
                System.out.println("hello");
                endPhase();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }


    public static void setCyclicBarrier( CyclicBarrier cb ) {
        GameService.cb = cb;
    }

    public static int[] getTotalVotes() {
        return totalVotesFromLastRound.clone();
    }


    public static CyclicBarrier getBarrier() {
        return GameService.cb;
    }

    public static void main(String[] args) {
        GameService.game = new Game( new Deck[] {   new Deck( Stock.Apple ),
                                        new Deck( Stock.BP ),
                                        new Deck( Stock.Cisco ),
                                        new Deck( Stock.Dell ),
                                        new Deck( Stock.Ericsson ) } );

        Request.game = GameService.game;

        GameService service = new GameService();

        service.connectPlayers( 1 );

        service.preGameActions();

        service.run();
    }
}
