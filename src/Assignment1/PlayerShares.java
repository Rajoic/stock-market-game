package Assignment1;

public class PlayerShares {

    private StocksPair stock;
    private int amountOwned = 0;

    public PlayerShares( StocksPair stock ) {
        this.stock = stock;
    }

    public PlayerShares( StocksPair stock, int amountOwned) {
        this.stock = stock;
        this.amountOwned = amountOwned;
    }

    public void sellStocks( int amount ) throws InvalidTransactionException {
        /*
        Sell a players stocks according to a given amount.
        Expected inputs:
            - int amount: the amount of stocks a player is looking to sell

        Expected outputs:
            - A boolean value: true if it was successful, false if it wasn't.
                               This transaction will fail if the player doesn't
                               own as many stocks as they're looking to sell.
         */

        if( amount > this.amountOwned || amount < 1 ) {
            throw new InvalidTransactionException();
        }

        this.amountOwned -= amount;
    }

    public void buyStocks( int amount ) throws NegativePurchaseException {
        /*
        Buy stocks for a player. Should increase the amount of stocks owned
        by the player by a given amount.

        Expected inputs:
            - int amount: the amount of stocks the player is looking to buy.
         */
        if( amount < 0 ) throw new NegativePurchaseException();
        this.amountOwned += amount;
    }

    public StocksPair getStockPair() {
        return this.stock;
    }

    public int getAmountOwned() {
        return this.amountOwned;
    }

    @Override
    public boolean equals( Object other ) {
        PlayerShares that = null;
        try {
            that = (PlayerShares) other;
        } catch ( ClassCastException e ) {
            return false;
        }

        if( this.amountOwned == that.amountOwned && this.getStockPair().equals( that.getStockPair() ) ) return true;
        return false;
    }

}

class NegativePurchaseException extends Exception {}

class InvalidTransactionException extends Exception {}
