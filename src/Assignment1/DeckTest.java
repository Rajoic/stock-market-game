package Assignment1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class DeckTest {

    Stocks stocks;

    @Before
    public void before() {
        stocks = new Stocks();
    }

    @Test
    public void constructorTest() {
        Deck deck;
        for( StocksPair pair : stocks.getStocks() ) {
            deck = new Deck( pair.getStock() );
            assertTrue( deck.getStock() == pair.getStock() );
            assertTrue( deck.toString().equals( pair.getStock().name() + " " + deck.cards ) );
            deck.shuffle();
            assertFalse( deck.toString().equals( pair.getStock().name() + " " + new Deck( pair.getStock() ).cards ));

            deck = new Deck( pair.getStock(), new int[]{ -20, -10, -5, 5, 10, 20 } );
            assertTrue( deck.getStock() == pair.getStock() );
            assertTrue( deck.toString().equals( pair.getStock().name() + " " + deck.cards ) );
            deck.shuffle();
            assertFalse( deck.toString().equals( pair.getStock().name() + " " + new Deck( pair.getStock() ).cards ));
        }
    }

    @Test
    public void shuffleTest() {
        Stock stock = stocks.getStocks()[0].getStock();
        Deck deck = new Deck( stocks.getStocks()[0].getStock() );
        deck.shuffle();
        assertFalse( deck.toString().equals( stock.name() + " " + new Deck( stock ).cards ));

        deck = new Deck( stock, new int[]{ -20, -10, -5, 5, 10, 20 } );
        deck.shuffle();
        assertTrue( deck.toString().equals( stock.name() + " " + deck.cards ) );
    }
}