package Assignment1;

import com.sun.istack.internal.NotNull;
import java.util.Arrays;

public class Request {

    public static Game game;
    public static Stocks stocks;

    public enum PlayerRequest {
        START_GAME,
        PLAYER_INFO,
        STOCK_INFO,
        BUY,
        SELL,
        VOTE,
        END_ROUND,
        INVALID,
        HELP
    }

    private String[] args;
    PlayerRequest request;

    public Request( PlayerRequest request, String ... args ) {
        this.args = args;
        this.request = request;
    }

    private static String[] insertId( int id, String[] oldArgs ) {
        String[] args = new String[ oldArgs.length + 1];
        args[0] = "" + id;
        for( int i = 1; i < args.length; i++ ) args[ i ] = oldArgs[ i - 1 ];
        return args;
    }

    public static Request parseRequest( @NotNull String rawRequest, int playerId ) throws BadRequestException {
        rawRequest = rawRequest.trim();
        String[] request = rawRequest.split( "\\s+" );


        if( request.length < 1 ) throw new BadRequestException();

        String newPlayerRequest = request[ 0 ].toUpperCase();
        switch( newPlayerRequest ) {
            case "START_GAME":
                return new Request( PlayerRequest.START_GAME, Arrays.copyOfRange(request, 1, request.length ) );
            case "PLAYER_INFO":
                return new Request( PlayerRequest.PLAYER_INFO, Arrays.copyOfRange(request, 1, request.length ) );
            case "STOCK_INFO":
                return new Request( PlayerRequest.STOCK_INFO, Arrays.copyOfRange(request, 1, request.length ) );
            case "BUY":
                return new Request( PlayerRequest.BUY, insertId( playerId, Arrays.copyOfRange(request, 1, request.length ) ) );
            case "SELL":
                return new Request( PlayerRequest.SELL, insertId( playerId, Arrays.copyOfRange(request, 1, request.length ) ) );
            case "VOTE":
                return new Request( PlayerRequest.VOTE, insertId( playerId, Arrays.copyOfRange(request, 1, request.length ) ) );
            case "HELP":
                return new Request( PlayerRequest.HELP, Arrays.copyOfRange(request, 1, request.length ) );
            case "END_ROUND":
                return new Request( PlayerRequest.END_ROUND, insertId( playerId, Arrays.copyOfRange(request, 1, request.length ) ) );
        }
        return new Request( PlayerRequest.INVALID, request );
    }

    public static String help() {
        String message = "Commands:" + System.lineSeparator();
        message = message + "   PLAYER_INFO {PlayerID} {PlayerID} ..." + System.lineSeparator();
        message = message + "   STOCK_INFO" + System.lineSeparator();
        message = message + "   BUY {Stock_Name} {Quantity}" + System.lineSeparator();
        message = message + "   SELL {Stock_Name} {Quantity}" + System.lineSeparator();
        message = message + "   VOTE {Stock_Name} {true/false}" + System.lineSeparator();
        message = message + "   END_ROUND" + System.lineSeparator();

        return message;
    }

    public static String executeRequest( Request request ) {
        String message = "";
        Stock stock;
        switch( request.request ) {
            case PLAYER_INFO:

                if( request.args.length < 1 )
                    return  "Error: Incorrect Usage.\n" +
                            "Send \"HELP\" for more information on performing actions.";
                Player player;
                int[] shares;

                for( String arg : request.args ) {
                    int num;
                    try {
                        num = Integer.parseInt(arg.trim());
                        if( num > GameService.maxPlayers ) continue;
                        player = game.getPlayer(num);
                    } catch( NumberFormatException e ) {
                        continue;
                    }
                    if( player == null ) continue;
                    message = message + "Player " + num + ":" + System.lineSeparator();
                    message = message + "    Balance: " + player.getBalance() + System.lineSeparator();
                    message = message + "    Stocks Owned: ";
                    shares = player.getPlayerSharesOwned();
                    int i = 0;
                    for( Stock stockVal : Stock.values() ) {
                        message = message + stockVal.name() + ": " + shares[i] + "  ~  ";
                        i++;
                    }
                }
                if( !message.equals( "" ) ) return message;
                return "Error: No players with those IDs were found.";

            case STOCK_INFO:
                for( StocksPair pair : stocks.getStocks() )
                    message = message + pair.getStock() + ": £" + pair.getValue() + System.lineSeparator();
                return message;

            case BUY:
                stock = Stock.parse( request.args[ 1 ].trim() );
                try {
                    return game.buy( Integer.parseInt( request.args[ 0 ].trim() ), stock, Integer.parseInt( request.args[ 2 ] ) );
                } catch( NumberFormatException e ) {
                    return "Error: Incorrect syntax." + System.lineSeparator() + "Usage: \"BUY {Stock_Name} {Quantity}\"";
                } catch( Exception e ) {
                    return  "Error: There was a problem processing your request. Try again." + System.lineSeparator() +
                            "Send \"HELP\" for more information on performing actions";
                }

            case SELL:
                stock = Stock.parse( request.args[ 1 ].trim() );
                try {
                    return game.sell( Integer.parseInt( request.args[ 0 ].trim() ), stock, Integer.parseInt( request.args[ 2 ].trim() ) );
                } catch( NumberFormatException e ) {
                    return "Error: Incorrect syntax.\nUsage: \"BUY {Stock_Name} {Quantity}\"";
                } catch( Exception e ) {
                    return  "Error: There was a problem processing your request. Try again." + System.lineSeparator() +
                            "Send \"HELP\" for more information on performing actions";
                }

            case VOTE:
                stock = Stock.parse( request.args[ 1 ].trim() );
                boolean vote;
                try {
                    String value = request.args[ 2 ].trim().toLowerCase();
                    if( value.equals( "true" ) ) vote = true;
                    else if( value.equals( "false" ) ) vote = false;
                    else break;

                    int id = Integer.parseInt( request.args[ 0 ].trim() );
                    game.vote( id, stock, vote );
                    return "Vote successful.";
                } catch( NumberFormatException e ) {
                    return "Error: Incorrect syntax." + System.lineSeparator() + "Usage: \"VOTE {Stock_Name} {true/false}";
                } catch( Exception e ) {
                    e.printStackTrace();
                    return "Error: There was a problem processing request. Try again.";
                }

            case INVALID:
                return "Error: Invalid request. Send \"HELP\" for more information on performing actions";

            case HELP:
                return help();

            case END_ROUND:
                return game.getPlayer( Integer.parseInt( request.args[ 0 ].trim() ) ).endRound();
        }
        return "Error: Syntax error. Send \"HELP\" for more information on performing actions";
    }

    public PlayerRequest getRequestType() {
        return this.request;
    }

    public String[] getArgs() {
        return this.args;
    }

    @Override
    public String toString() {
        StringBuilder requestBuilder = new StringBuilder();
        requestBuilder.append( this.request );
        if( args == null || args.length < 1 ) return requestBuilder.toString();
        for( String arg : args ) requestBuilder.append( " " + arg );
        return requestBuilder.toString();
    }

    @Override
    public boolean equals( Object thatObj ) {
        Request that;
        try {
            that = (Request) thatObj;
        } catch( ClassCastException e ) {
            return false;
        }

        if( Arrays.deepEquals( this.args, that.args ) && this.request.equals( that.request ) )
            return true;

        return false;
    }
}

class BadRequestException extends Exception {}
