package Assignment1;

import org.junit.Test;

import static org.junit.Assert.*;

public class RequestTest {

    @Test
    public void parseRequestTest() {
        Request request;
        String requestString = "PLAYER_INFO 1";
        Stocks s = new Stocks();
        Player p = new Player( 1, s.setPlayerShares() );

        try {
            request = Request.parseRequest( requestString, p.getPlayerId() );
            assertTrue( request.equals( new Request( Request.PlayerRequest.PLAYER_INFO, "1" ) ) );
            assertTrue( request.toString().equals( requestString ) );
        } catch (BadRequestException e) {
            e.printStackTrace();
        }

        String playerRequest = "Stock_Info";
        String[] args = new String[] { "1", "2", "3" };

        requestString = playerRequest;
        for( String arg : args ) requestString = requestString + " " + arg;
        try {
            request = Request.parseRequest( requestString, p.getPlayerId() );


            assertTrue( request.equals( new Request( Request.PlayerRequest.STOCK_INFO, args ) ) );
            assertTrue( request.toString().equals( new Request( Request.PlayerRequest.STOCK_INFO, args ).toString() ) );
        } catch (BadRequestException e) {
            e.printStackTrace();
        }
    }
}