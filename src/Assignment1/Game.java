package Assignment1;

import java.util.ArrayList;

public class Game {

    public static boolean startGame = false;
    private ArrayList< Player > players;
    private Stocks stocks = new Stocks();

    Deck[] decks;
    ArrayList< Card > visibleCards;

    public Game( Deck[] decks ) {
        this.decks = decks;
        this.startGame = false;
    }

    public Game( Deck[] decks, int[][] shares ) {
        this.decks = decks;
        this.startGame = false;
        for( int i = 0; i < shares.length; i++ ) {
            addPlayer( new Player( i, this.stocks.setPlayerShares( shares[ i ] ) ) );
        }
    }

    public void drawCards() {
        this.visibleCards = new ArrayList<>();
        for( Deck deck : this.decks ) {
            this.visibleCards.add( deck.drawCard() );
        }
    }

    public void drawCards( int[] votes ) {

        ArrayList< Card > newCards = new ArrayList<>();

        for( int i = 0; i < decks.length; i++ ) {
            if( votes[ i ] == 0 ) newCards.add( this.visibleCards.get( i ) );
            else newCards.add( this.decks[ i ].drawCard() );
        }

        this.visibleCards = newCards;
    }

    public String sell( int playerId, Stock stock, int amount ) {
        try {
            this.players.get( playerId - 1 ).sellStocks( stock, amount );
            return "Transaction successful.";
        } catch (InvalidStockException e) {
            e.printStackTrace();
            return "Error: The stock you entered doesn't exist.";
        } catch (InvalidQuantityException e) {
            e.printStackTrace();
            return "Error: You cannot sell less that 1 share or more shares than you own.";
        } catch (ActionsExhaustedException e) {
            e.printStackTrace();
            return "Error: You've already performed your 2 actions for this round.";
        }
    }

    public String buy( int playerId, Stock stock, int amount ) {
        try {
            this.players.get( playerId - 1 ).buyStocks( stock, amount );
            return "Transaction successful.";
        } catch (InvalidQuantityException e) {
            e.printStackTrace();
            return "Error: You cannot buy less that 1 share.";
        } catch (InvalidStockException e) {
            e.printStackTrace();
            return "Error: The stock you entered doesn't exist.";
        } catch (InadequateFundsException e) {
            e.printStackTrace();
            return "Inadequate funds. Purchase rejected.";
        } catch (ActionsExhaustedException e) {
            e.printStackTrace();
            return "Error: You've already performed your 2 actions for this round.";
        }
    }

    public int[] getShares( int playerId ) {
        return this.players.get( playerId - 1 ).getPlayerSharesOwned() ;
    }

    public int getCash( int playerId ) {
        return this.players.get( playerId - 1 ).getBalance();
    }

    public String vote( int playerId, Stock stock, boolean vote ) {
        try {
            this.players.get( playerId - 1).vote( stock, vote );
            return "Vote successful";
        } catch (VotesExhaustedException e) {
            e.printStackTrace();
            return "Error: You have no remaining votes this round.";
        }
    }

    public int[] getTotalVotes() {
        int[] votes = new int[] { 0, 0, 0, 0, 0 };
        int[] playerVotes;

        for( Player player: this.players ) {
            playerVotes = player.getVotes();

            for( int vote = 0; vote < votes.length; vote++) {
                votes[ vote ] += playerVotes[ vote ];
            }
        }

        return votes;
    }

    public void executeVotes( int[] votes ){

        int i = 0;
        int effect = 0;

        for( int finalVotes : votes ) {
            if( finalVotes <= 0 ) {
                i++;
                continue;
            }
            if( finalVotes > 0 ) {
                effect = visibleCards.get( i ).getEffect();
                System.out.println( i + " effect " + effect);
                this.stocks.getPair( this.decks[ i ].getStock() ).applyEffect( effect );
            }
            i++;
        }

        this.drawCards( votes );
    }

    public int[] getPrices() {
        int[] prices = new int[5];
        StocksPair[] stocks = this.stocks.getStocks();
        for( int i = 0; i < 5; i++) {
            prices[ i ] = stocks[ i ].getValue();
        }
        return prices;
    }

    public Card[] getCards() {
        Card[] cards = new Card[5];
        int i = 0;
        for( Card card : visibleCards ) {
            cards[i] = card;
            i++;
        }
        return cards;
    }

    public Player getPlayer( int playerId ) {
        if( playerId - 1 > this.players.size() ) return null;
        return this.players.get( playerId -1 );
    }

    public void addPlayer( Player player ) {
        if( this.players == null ) this.players = new ArrayList<>();
        this.players.add( player );
    }
}
