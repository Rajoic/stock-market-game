package Assignment1;

import org.junit.Test;
import static org.junit.Assert.*;

public class CardsTest {

    Card card = new Card( -20 );

    @Test
    public void constructorTest() {
        assertTrue( card.equals( card ) );
        assertTrue( card.equals( new Card( -20 ) ) );
        assertFalse( card.equals( null ) );
        assertTrue( card.toString().equals( "-20" ) );
        assertTrue( card.toString().equals( new Card( -20 ).toString() ) );

    }

    @Test
    public void getEffectTest() {
        assertTrue( card.getEffect() == -20 );
        assertFalse( card.getEffect()== 20 );
    }

    @Test
    public void parseTest() {
        assertTrue( card.equals( Card.parse( "-20" ) ) );
        assertNotNull( Card.parse( " -10   ") );
        assertNull( Card.parse( "a" ) );
    }
}
