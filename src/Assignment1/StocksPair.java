package Assignment1;

public class StocksPair {

    private Stock stock;
    private int value;

    public StocksPair( Stock stock ) {
        /*
        Creates a StockPair using a given name and sets its initial value to 100.

        Expected inputs:
            - String stock: a String containing the name of the stock.
         */
        this.stock = stock;
        this.value = 100;
    }

    public Stock getStock() {
        /*
        Returns the name of this stock
         */
        return this.stock;
    }

    public int getValue() {
        /*
        Returns the current value of this stock
         */
        return this.value;
    }

    public void applyEffect( int effect ) {
        /*
        Applies an effect to the value of this stock.
        Expected inputs:
            - int effect: a value which holds an effect to be applied to the current stock;
         */
        this.value += effect;
    }

    @Override
    public boolean equals( Object stock ) {
        StocksPair that = null;
        try {
            that = ( StocksPair ) stock;
        } catch ( ClassCastException e ) {
            return false;
        }

        if( this.stock.equals( that.stock ) && this.value == that.value ) return true;
        return false;
    }
}
