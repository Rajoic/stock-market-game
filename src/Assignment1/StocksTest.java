package Assignment1;

import org.junit.Test;

import static org.junit.Assert.*;


public class StocksTest {

    Stocks stocks = new Stocks();
    StocksPair pair = new StocksPair( Stock.Apple );

    @Test
    public void stocksConstructorTest() {
        assertTrue(
                stocks.getPair( Stock.Apple ).equals( new StocksPair( Stock.Apple ) ) &&
                        stocks.getPair( Stock.BP ).equals( new StocksPair( Stock.BP ) ) &&
                        stocks.getPair( Stock.Cisco ).equals( new StocksPair( Stock.Cisco ) ) &&
                        stocks.getPair( Stock.Dell ).equals( new StocksPair( Stock.Dell ) ) &&
                        stocks.getPair( Stock.Ericsson ).equals( new StocksPair( Stock.Ericsson ) )
        );
    }

    @Test
    public void stockParseTest() {
        assertTrue( Stock.parse( "abfjndsahjkfda" ).equals( Stock.Apple ) );
        assertTrue( Stock.parse( "a" ).equals( Stock.Apple ) );
    }

    @Test
    public void stocksPairConstructorTest() {
        assertTrue( pair.equals( new StocksPair( Stock.Apple) ) );
        assertFalse( pair.equals( new StocksPair( Stock.BP ) ) );
    }

    @Test
    public void stocksPairGetStocksTest() {
        assertTrue( pair.getStock().equals( Stock.Apple ) );
        assertFalse( pair.getStock().equals( Stock.BP ) );
    }

    @Test
    public void stocksPairSetPlayerSharesTest() {
        PlayerShares[] shares = stocks.setPlayerShares( new int[] { 5, 5, 5, 5, 5 } );
        StocksPair[] pairs = stocks.getStocks();
        for( PlayerShares share : shares ) assertTrue( share.getAmountOwned() == 5 );
        for( int i = 0; i < 5; i++ ) assertTrue( shares[i].getStockPair().equals( pairs[i] ) );
    }

    @Test
    public void stocksPairValueTest() {
        assertTrue( pair.getValue() == 100 );

        pair.applyEffect( -10 );
        assertTrue( pair.getValue() == 90 );

        pair.applyEffect( 20 );
        assertTrue( pair.getValue() == 110 );
    }
}