package Assignment1;

public class Player {

    private int playerId;
    private int funds;
    private PlayerShares[] playerShares;
    private int actions = 2;
    private int votesRemaining = 2;
    private int[] votes = new int[] { 0, 0, 0, 0, 0 };
    private boolean concedeRound = false;

    public Player( int playerId, PlayerShares[] playerShares ) {
        this.playerId = playerId;
        this.funds = 500;
        this.playerShares = playerShares;
    }

    public void resetRound() {
        this.resetActions();
        this.resetVotes();
        concedeRound = false;
    }

    private void resetActions() {
        this.actions = 2;
    }

    private void resetVotes() {
        this.votesRemaining = 2;
        this.votes = new int[] { 0, 0, 0, 0, 0 } ;
    }

    public String endRound() {
        concedeRound = true;
        return "Skipping the rest of the round. Please wait for the other players...";
    }

    public boolean hasEndedRound() {
        return this.concedeRound;
    }

    public void vote( Stock stock, boolean vote ) throws VotesExhaustedException {
        PlayerShares playerShare;
        if( this.votesRemaining < 1) throw new VotesExhaustedException();
        for( int i = 0; 0 < playerShares.length; i++ ) {
            playerShare = this.playerShares[ i ];
            if( playerShare.getStockPair().getStock().equals( stock ) ) {
                if (vote) votes[i]++;
                else votes[i]--;
                votesRemaining--;
                break;
            }
        }
    }

    public int getActionsRemaining() { return this.actions; }

    public int getVotesRemaining() { return this.votesRemaining; }

    public int[] getVotes() {
        return this.votes.clone();
    }

    public int getBalance() {
        return this.funds;
    }

    public int getPlayerId() {
        return this.playerId;
    }

    public PlayerShares[] getPlayerShares() {
        return this.playerShares;
    }

    public int[] getPlayerSharesOwned() {
        int[] sharesOwned = new int[ 5 ];
        for( int i = 0; i < this.playerShares.length; i++ ) sharesOwned[ i ] = playerShares[ i ].getAmountOwned();
        return sharesOwned;
    }

    public void buyStocks( Stock stock, int amount )
            throws InvalidQuantityException, InvalidStockException, InadequateFundsException, ActionsExhaustedException {
        /*
        Assignment1.Player purchases stock using funds. This will increase the amount of stocks owned and
        decrease the player's funds. Cannot purchase stocks that value more than the players
        current funds.

        Expected inputs:
            - Assignment1.Stock stock: the stock the player wished to purchase.
            - int amount: the quantity of stock the player wishes to buy.
         */

        if( this.actions < 1 ) throw new ActionsExhaustedException();

        if( stock == null )
            throw new InvalidStockException();

        PlayerShares shares = null;

        for( PlayerShares playerShares : this.playerShares )
            if( playerShares.getStockPair().getStock().equals( stock ) ) shares = playerShares;

        if( amount < 1 )
            throw new InvalidQuantityException();

        int transactionCost = ( shares.getStockPair().getValue() * amount )
                                + ( amount * 3 );

        if( transactionCost > this.funds )
            throw new InadequateFundsException();

        this.funds -= transactionCost;
        try {
            shares.buyStocks( amount );
        } catch( Exception e ) {
            e.printStackTrace();
        }

        this.actions--;
    }

    public void sellStocks( Stock stock, int amount )
            throws InvalidStockException, InvalidQuantityException, ActionsExhaustedException {
        /*
        Assignment1.Player sells stock. This will increase a players funds in exchange for owned stocks.

        Expected inputs:
            - int stock: a number representing the stock the player wishes to sell.
            - int amount: the quantity of stock to be sold
         */

        if( actions < 1 ) throw new ActionsExhaustedException();

        if( stock == null )
            throw new InvalidStockException();

        PlayerShares shares = null;

        for( PlayerShares playerShares : this.playerShares ) {
            if (playerShares.getStockPair().getStock().equals(stock)) shares = playerShares;
        }

        if( amount < 1 || amount > shares.getAmountOwned() )
            throw new InvalidQuantityException();

        this.funds += shares.getStockPair().getValue() * amount;
        try {
            shares.sellStocks( amount );
        } catch( Exception e ) {
            e.printStackTrace();
        }

        this.actions--;
    }
}

class InvalidStockException extends Exception {}

class InvalidQuantityException extends Exception {}

class InadequateFundsException extends Exception {}

class ActionsExhaustedException extends Exception {}

class VotesExhaustedException extends Exception {}