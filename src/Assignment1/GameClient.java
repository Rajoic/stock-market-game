package Assignment1;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.CyclicBarrier;

public class GameClient extends Thread {

    private static Stocks stocks;
    private static int round = 1;
     // private static CyclicBarrier cb;
    private static int maxPlayers;

    private Socket playerSocket;
    private Player player;
    private Game game;

    private Scanner in;
    private PrintWriter out;

    public boolean connected = true;
    private static boolean drawn = false;

    public static void draw( Game game, int[] votes ) {

        if( !drawn ) {
            System.out.println( "Draw " + Arrays.toString( votes ) );
            game.drawCards( votes );
            drawn = true;
        }
    }

    public GameClient(Socket socket, int id, PlayerShares[] playerShares, Game game ) throws IOException {
        this.playerSocket = socket;
        this.player = new Player( id, playerShares );
        this.game = game;

        game.addPlayer( this.player );

        this.in = new Scanner( this.playerSocket.getInputStream() );
        this.out = new PrintWriter( this.playerSocket.getOutputStream(), true );
    }

    public String getRoundState() {
        ArrayList< Card > visibleCards = game.visibleCards;
        StocksPair[] shares = stocks.getStocks();

        String message =    System.lineSeparator();

        message = message + "    ###########" + System.lineSeparator() +
                            "    # Round " + round + " #" + System.lineSeparator() +
                            "    ###########" + System.lineSeparator() + System.lineSeparator();

        message = message + "Your statistics:" + System.lineSeparator() + "----------------" + System.lineSeparator() +
                Request.executeRequest( new Request(
                Request.PlayerRequest.PLAYER_INFO, "" + player.getPlayerId() ) ) +
                System.lineSeparator()  + "    Actions Remaining: " + player.getActionsRemaining() + System.lineSeparator() +
                "    Votes Remaining: " + player.getVotesRemaining() + System.lineSeparator() + System.lineSeparator();

        message = message + "Cards in play: " + System.lineSeparator() + "    ";
        for( int i = 0; i < 5; i++ ) {
            System.out.println(shares[i]);
            System.out.println(shares[i].getStock());
            System.out.println(visibleCards.get(i).getEffect());
            message = message + shares[ i ].getStock() + " " + visibleCards.get( i ).getEffect() + "  ~  ";
        }

        message = message + System.lineSeparator();
        return message;
    }

    public void executeRound() {
        player.resetRound();
        while( ( player.getVotesRemaining() > 0 || player.getActionsRemaining() > 0 ) && !player.hasEndedRound() ) {
            System.out.println( 2);
            out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            out.println( getRoundState() );
            out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            out.print( System.lineSeparator() + "Input a request: ");
            out.flush();

            String request = in.nextLine();
            try {
                Request r = Request.parseRequest( request, player.getPlayerId() );
                out.println( System.lineSeparator() + Request.executeRequest( r ) + System.lineSeparator() );
                out.flush();
            } catch ( BadRequestException e ) {
                e.printStackTrace();
                out.println( "Error: Bad request. Type \"HELP\" for more information on making requests." + System.lineSeparator());
            }
        }
        try { GameService.getBarrier().await(); } catch (Exception e) { e.printStackTrace(); }
        try { Thread.sleep( 100 ); } catch ( Exception e ) {}
    }

    private void askMaxPlayers() {
        while ( maxPlayers < 1 || maxPlayers > 4 ) {
            out.print( System.lineSeparator() + "Input how many players you would like in this game: ");
            out.flush();
            try {
                GameClient.maxPlayers = GameService.maxPlayers = Character.getNumericValue( in.nextLine().trim().charAt( 0 ) );
                out.print( System.lineSeparator() );
            } catch( Exception e ) {
                out.print("Error: That wasn't a number. Please input a number between 1 and 4." + System.lineSeparator() );
            }
            if (maxPlayers < 1 || maxPlayers > 4) {
                out.print("Error: That wasn't a number between 1 and 4.");
            }
        }
        if( maxPlayers > 1 ) out.println( "Waiting for more players..." );
        try { GameService.getBarrier().await(); } catch ( Exception e ) { e.printStackTrace(); }

        GameService.setCyclicBarrier( new CyclicBarrier( maxPlayers + 1 ) );
    }

    public void gameSummary() {
        out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        out.print( System.lineSeparator() + "                                Game summary!" + System.lineSeparator() + System.lineSeparator());
        out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        int winner = 0;
        int winnerFunds = 0;
        for( int i = 1; i <= maxPlayers; i++ ) {
            int funds = game.getPlayer( i ).getBalance();
            if( funds > winnerFunds ) {
                winner = i;
                winnerFunds = funds;
            }
            out.println( "Player 1 funds: " + funds );
        }
        out.println( System.lineSeparator() + "Winner: Player " + winner );
    }

    public void roundSummary() {
        int[] votes = GameService.getTotalVotes();
        out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        out.print( System.lineSeparator() + "                              Round " + ( round - 1 ) + " summary!" + System.lineSeparator() + System.lineSeparator());
        out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        StocksPair[] pairs = stocks.getStocks();
        out.println("    Total Votes:");
        for( int i = 0; i < 5; i++ ) {
            if( votes[ i ] > 0 ) {
                out.println( pairs[ i ].getStock() + " : " + votes[ i ]  + "    CARD EFFECT APPLIED!");
                pairs[i].applyEffect( game.visibleCards.get( i ).getEffect() );
            }
            else if( votes[ i ] == 0 ) out.println( pairs[ i ].getStock() + " : " + votes[ i ] );
            else out.println( pairs[ i ].getStock() + " : " + votes[ i ] + "    CARD DISCARDED!");
        }
        draw( game, votes );
        out.println( Request.executeRequest( new Request( Request.PlayerRequest.STOCK_INFO, "" ) ) );

    }

    @Override
    public void run() {
        try {
            out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            out.print( System.lineSeparator() + "                              Welcome player " + (player.getPlayerId()) + "!" + System.lineSeparator() + System.lineSeparator());
            out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            if (player.getPlayerId() == 1) {
                askMaxPlayers();
            }
            System.out.println( 0 );
            System.out.println( GameService.getBarrier().getParties() + " " + GameService.getBarrier().getNumberWaiting() );
            try { GameService.getBarrier().await(); } catch ( Exception e ) { e.printStackTrace(); }
            out.print( Request.help() + System.lineSeparator() );

            while (round < 6) {
                System.out.println( 1 );
                executeRound();
                try { GameService.getBarrier().await(); } catch ( Exception e ) { e.printStackTrace(); }
                roundSummary();
            }
            gameSummary();

            try {
                Thread.sleep(10000);
                playerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch( NoSuchElementException e ) {
            this.connected = false;
        }
    }

    public static void setMaxPlayers( int maxPlayers ) {
        GameClient.maxPlayers = maxPlayers;
    }

    /*
    public static void setBarrier( CyclicBarrier cb ) {
        GameClient.cb = cb;
    }
    */

    public static void setStocks( Stocks stocks ) {
        GameClient.stocks = stocks;
    }

    public static void setRound( int round ) {
        GameClient.round = round;
    }

    public static void incrementRound() {
        GameClient.round++;
    }
}
